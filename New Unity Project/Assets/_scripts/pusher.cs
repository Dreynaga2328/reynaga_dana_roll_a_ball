﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pusher : MonoBehaviour
{
    Rigidbody rb;
    public float pushSpeed; // sets the speed of the block

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Invoke("Destroy",20f); // waits 20 seconds to destory the object
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.velocity = transform.forward * pushSpeed; // sets the velocity for going forward
    }

    void Destroy()
    {
    Destroy(gameObject); // delete ourselves

    }
    
}
