﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pusherspaw : MonoBehaviour
{
    public GameObject pusherPrefab;
    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("CreatePusher", 20f, 20f); // will call the function after 20 seconds 
    }

    // Update is called once per frame
    void CreatePusher()
    {
        //the pusher prefact at our position and give it the roation allosw the push spawner to move directions
        Instantiate(pusherPrefab, transform.position, transform.rotation);
    }


}

