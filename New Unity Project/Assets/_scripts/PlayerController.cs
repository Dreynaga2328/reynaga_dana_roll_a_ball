﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;



public class PlayerController : MonoBehaviour
{
    public float speed;
    public Text countText;
    public Text winText;
    public Text timerText;

    float timer;




    private Rigidbody rb;
    private int count;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        SetTimerText();
        winText.text = "";
        
    }


    void Update()
    {
       timer += Time.deltaTime;
        SetTimerText();
       if (timer >= 30f) // sets the timer
        {
            print("Game Over");
        }

       
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVertical);


        rb.AddForce(movement * speed);

        {
            //makes the ball jump
            if (Input.GetKeyDown("space") && GetComponent<Rigidbody>().transform.position.y <= 0.6250001f)
            {
                Vector3 jump = new Vector3(0.0f, 200.0f, 0.0f);

                // gets the component 
                GetComponent<Rigidbody>().AddForce(jump);
            }
        }


    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
     
    }

 

    void SetCountText()
    {
        countText.text = "Count:" + count.ToString();
        if (count >= 15)
        {
            winText.text = "You Win";
        }
    }

    void SetTimerText() // timer added 
    {
        timerText.text = "Timer" + timer.ToString();
     
        
    }
       
}


